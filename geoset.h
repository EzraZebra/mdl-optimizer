#ifndef GEOSET_H
#define GEOSET_H

#include "extent.h"

#include <vector>
#include <memory>

struct Matrix
{
    size_t index;
    std::vector<size_t> matrix;
};

struct Vertex
{
    size_t index;
    std::array<QByteArray, 3> vertex, normal;
    std::array<QByteArray, 2> tVertex;
    Matrix *group; // Points to an element of the `matrices` vector in Geoset
};

class Geoset
{
    // Geoset data
    Common *c;
    std::vector<std::unique_ptr<Vertex>> vertices;
    std::vector<std::unique_ptr<Matrix>> matrices;
    std::vector<Vertex*> triangles; // Points to an element of `vertices`
    std::vector<Extent*> anims;
    Extent *extent;
    int materialID=-1, selectionGroup=-1;
    bool unselectable = false;

    // Parsing data
    QString object, prop;
    size_t objIndex = 0;
    QString error_;

    // Optimization
    int diffTris_ = 0, diffVtcs_ = 0, diffMtcs_ = 0, diffSeqExts_ = 0;
    void deleteFakeTriangles();
    void deleteFreeVertices();
    void mergeIdenticalVertices();
    void deleteUnusedMatrices();

public:
    Geoset(Common *c) : c(c), extent(new Extent(c)) {}

    // Getters
    inline int diffTris() const { return diffTris_; }
    inline int diffVtcs() const { return diffVtcs_; }
    inline int diffMtcs() const { return diffMtcs_; }
    inline int diffSeqExts() const { return diffSeqExts_; }
    inline const QString &error() const { return error_; }

    // Process
    bool line(QByteArray &line);
    bool optimize(bool fakeTris, bool freeVtcs, bool idVtcs, bool unusedMtcs, int seqExts);
    const QByteArrayList print() const;
};

#endif // GEOSET_H
