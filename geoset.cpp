#include "geoset.h"

/** Convert line to Geoset data. Expects a trimmed line.
 * Return true on error or when last line is reached. */
bool Geoset::line(QByteArray &line)
{
    // Close the current scope
    if (line == "}") {
        if (!prop.isEmpty()) prop = "";
        else if (!object.isEmpty()) {
            object = "";
            objIndex = 0;
        }
        else return true;

        return false;
    }

    bool unknownObj = false;
    int spacePos = line.indexOf(" ");
    QByteArray tmpObj = line.right(line.size()-spacePos-1);
    QByteArrayList list = Common::getList(tmpObj);
    tmpObj = line.left(spacePos);

    // Open a new object
    if (object.isEmpty()) {
        if (tmpObj == "MaterialID") materialID = list.first().toInt();
        else if (tmpObj == "SelectionGroup") selectionGroup = list.first().toInt();
        else if (line.startsWith("Unselectable")) unselectable = true;
        else if (!extent->line(tmpObj, list)){
            if (tmpObj == "Anim") anims.push_back(new Extent(c)); // Add new animation extent
            object = tmpObj;
        }
    }
    else if (object == "Groups") {
        if (tmpObj == "Matrices") {
            // Fill matrices vector to current objIndex if necessary
            for (size_t i = matrices.size(); i <= objIndex; i++) {
                matrices.push_back(std::unique_ptr<Matrix>(new Matrix({ .index = i, .matrix = {} })));
            }

            for (auto const& id : list)  matrices.at(objIndex)->matrix.push_back(id.toULongLong());
            objIndex++;
        }
        else {
            prop = tmpObj;
            unknownObj = true;
        }
    }
    else if (object == "Anim") {
        if (!anims.back()->line(tmpObj, list)) {
            prop = tmpObj;
            unknownObj = true;
        }
    }
    else {
        list = Common::getList(line);

        if (object == "Faces") {
            if (prop == "Triangles") {
                for (auto const& id : list) triangles.push_back(vertices.at(id.toULongLong()).get());
            }
            else if (prop.isEmpty() && tmpObj == "Triangles") prop = tmpObj;
            else unknownObj = true;
        }
        else {
            // Current objIndex exceeds vertices size -> return error
            if (objIndex > vertices.size()) {
                error_ = "Geoset " + object + " index out of bounds (" + QString::number(objIndex) + ") ";
                return true;
            }

            // Add new element to vertices
            if (objIndex == vertices.size()) {
                Vertex *newVtx = new Vertex({ .index = objIndex, .vertex = {}, .normal = {}, .tVertex = {}, .group = nullptr });
                vertices.push_back(std::unique_ptr<Vertex>(newVtx));
            }

            if (object == "Vertices")
                vertices.at(objIndex)->vertex = { c->round(list.first()), c->round(list.at(1)), c->round(list.last()) };
            else if (object == "Normals")
                vertices.at(objIndex)->normal = { c->round(list.first()), c->round(list.at(1)), c->round(list.last()) };
            else if (object == "TVertices")
                vertices.at(objIndex)->tVertex = { c->round(list.first()), c->round(list.last()) };
            else if (object == "VertexGroup") {
                size_t index = list.first().toULongLong();

                // Fill matrices vector to index if necessary
                for (size_t i = matrices.size(); i <= index; i++)
                    matrices.push_back(std::unique_ptr<Matrix>(new Matrix({ .index = i, .matrix = {} })));

                vertices.at(objIndex)->group = matrices.at(index).get();
            }
            else unknownObj = true;

            objIndex++;
        }
    }

    if (unknownObj) {
        error_ = "Unknown Geoset object: " + object;
        if (!prop.isEmpty()) error_ += " - " + prop;
        return true;
    }

    return false;
}

/** Delete triangles that don't have three unique vertices.
 * Leave at least one triangle to avoid deleting a geoset. */
void Geoset::deleteFakeTriangles()
{
    for (auto it = triangles.begin(); it != triangles.end() && triangles.size() > 3; ) {
        auto second = std::next(it);
        auto third = std::next(second);
        if (*it == *second || *it == *third || *second == *third) {
            triangles.erase(it);
            triangles.erase(it);
            triangles.erase(it);
            diffTris_++;
        }
        else it = std::next(third);
    }
}

/** Delete vertices that aren't part of a triangle.
 * Delete matrices that are no longer used after this. */
void Geoset::deleteFreeVertices()
{
    // For each vertex, find a triangle that uses it
    size_t i = 0;
    for (auto it = vertices.begin(); it != vertices.end(); ) {
        auto itResult = std::find(triangles.begin(), triangles.end(), it->get());
        if (itResult == triangles.end()) {
            vertices.erase(it);
            diffVtcs_++;
        }
        else {
            it->get()->index = i;
            i++;
            it++;
        }
    }
}

/** Merge vertices with the same vertex, normal and texture vertex coordinates. */
void Geoset::mergeIdenticalVertices()
{
    // Loop through all vertices
    for (auto ndlIt = vertices.begin(); ndlIt != vertices.end(); ndlIt++) {
        Vertex *ndl = ndlIt->get();
        std::vector<Matrix*> checkGrps = { ndl->group };
        Matrix newGrp = *(ndl->group);

        // Loop through to all subsequent vertices to find matches
        size_t i = ndl->index+1;
        for (auto stkIt = std::next(ndlIt); stkIt != vertices.end(); ) {
            Vertex *stk = stkIt->get();

            // If vertex matches
            if (ndl->vertex == stk->vertex && ndl->normal == stk->normal && ndl->tVertex == stk->tVertex) {
                // Replace triangle references
                for (auto trIt = triangles.begin(); trIt != triangles.end(); ) {
                    trIt = std::find(trIt, triangles.end(), stk);
                    if (trIt != triangles.end()) {
                        *trIt = ndl;
                        trIt ++;
                    }
                }

                // Group is different from previous vertex matches -> merge into new group
                if (std::find(checkGrps.begin(), checkGrps.end(), stk->group) == checkGrps.end()) {
                    int i = 0;
                    for (auto newIt = newGrp.matrix.begin(), checkIt = stk->group->matrix.begin();
                         checkIt != stk->group->matrix.end(); ) {
                        if (newIt == newGrp.matrix.end() || *checkIt <= *newIt) {
                            if (newIt == newGrp.matrix.end())
                                newGrp.matrix.push_back(*checkIt);
                            else if (*checkIt < *newIt) {
                                newGrp.matrix.insert(newIt, *checkIt);
                                i++;
                                newIt = newGrp.matrix.begin()+i;
                            }

                            checkIt++;
                        }
                        else {
                            i++;
                            newIt++;
                        }
                    }

                    checkGrps.push_back(stk->group);
                }

                vertices.erase(stkIt);
                diffVtcs_++;
            }
            else {
                stk->index = i;
                i++;
                stkIt++;
            }
        }

        // At least one different group was found
        if (checkGrps.size() > 1) {
            // Look for an identical existing group
            bool match = false;
            for (auto const& mtx : matrices) {
                if (mtx->matrix == newGrp.matrix) {
                    ndl->group = mtx.get();
                    match = true;
                    break;
                }
            }

            // No match -> assign new group and add to matrices
            if (!match) {
                newGrp.index = matrices.size();
                matrices.push_back(std::unique_ptr<Matrix>(&newGrp));
                ndl->group = &newGrp;
                diffMtcs_--;
            }
        }
    }
}

/** Delete matrices not connected to any vertices */
void Geoset::deleteUnusedMatrices()
{
    // For each matrix group, find a vertex that uses it and delete if none is found
    size_t i = 0;
    for (auto it = matrices.begin(); it != matrices.end(); ) {
        bool inUse = false;
        for (auto const& vtx : vertices) {
            if (vtx->group == it->get()) {
                inUse = true;
                break;
            }
        }

        if (!inUse) {
            matrices.erase(it);
            diffMtcs_++;
        }
        else {
            it->get()->index = i;
            i++;
            it++;
        }
    }
}

/** Optimize model and return whether changes have been made */
bool Geoset::optimize(bool fakeTris, bool freeVtcs, bool idVtcs, bool unusedMtcs, int seqExts)
{
    int trisB = diffTris_, vtcsB = diffVtcs_, mtcsB = diffMtcs_, seqExtsB = diffSeqExts_;

    if (fakeTris) deleteFakeTriangles();
    if (freeVtcs) deleteFreeVertices();
    if (idVtcs) mergeIdenticalVertices();
    if (unusedMtcs) deleteUnusedMatrices();
    if (seqExts >= 0 && seqExts < (int)anims.size()) {
        diffSeqExts_ = anims.size() - seqExts;
        anims.resize(seqExts);
    }

    return trisB != diffTris_ || vtcsB != diffVtcs_ || mtcsB != diffMtcs_ || seqExtsB != diffSeqExts_;
}

/** Return a QByteArrayList with the geoset for outputting to a .mdl file */
const QByteArrayList Geoset::print() const
{
    QByteArrayList print;
    QByteArray vtcsSize = QByteArray::number(vertices.size()), vtcs, nrmls, tvtcs, vgrps, mtcs;

    // Loop through vertex data and format each property
    for (auto const& vtx : vertices) {
        vtcs +=     "\t\t{ " + vtx->vertex.at(0) + ", " + vtx->vertex.at(1) + ", " + vtx->vertex.at(2) + " },\n";
        nrmls +=    "\t\t{ " + vtx->normal.at(0) + ", " + vtx->normal.at(1) + ", " + vtx->normal.at(2) + " },\n";
        tvtcs +=    "\t\t{ " + vtx->tVertex.at(0) + ", " + vtx->tVertex.at(1) + " },\n";
        vgrps +=    "\t\t" + QByteArray::number(vtx->group->index) + ",\n";
    }

    print += "Geoset {\n";

    print += "\tVertices " + vtcsSize + " {\n";
    print += vtcs;
    print += "\t}\n";

    print += "\tNormals " + vtcsSize + " {\n";
    print += nrmls;
    print += "\t}\n";

    print += "\tTVertices " + vtcsSize + " {\n";
    print += tvtcs;
    print += "\t}\n";

    print += "\tVertexGroup {\n";
    print += vgrps;
    print += "\t}\n";

    // Triangle data
    print += "\tFaces 1 " + QByteArray::number(triangles.size()) + " {\n";
    print += "\t\tTriangles {\n";
    print += "\t\t\t{ ";
    for (auto vtx = triangles.begin(); vtx != triangles.end(); vtx++) {
        print += QByteArray::number((*vtx)->index);
        if (std::next(vtx) != triangles.end()) print += ", ";
    }
    print += " },\n";
    print += "\t\t}\n";
    print += "\t}\n";

    // Loop through matrices data
    size_t mtxSize = 0;
    for (auto const& grp : matrices) {
        mtcs += "\t\tMatrices { ";
        for (auto mtx = grp->matrix.begin(); mtx != grp->matrix.end(); mtx++) {
            mtcs += QByteArray::number(*mtx);
            if (std::next(mtx) != grp->matrix.end()) mtcs += ", ";
            mtxSize++;
        }
        mtcs += " },\n";
    }
    print += "\tGroups " + QByteArray::number(matrices.size()) + " " + QByteArray::number(mtxSize) + " {\n";
    print += mtcs;
    print += "\t}\n";

    // Extents/bounds
    QByteArrayList ext = extent->print("\t");
    if (!ext.isEmpty()) print += ext;

    // Sequence extents/bounds
    for (auto const& anim : anims) {
        ext = anim->print("\t\t");
        if (!ext.isEmpty()) {
            print += "\tAnim {\n";
            print += anim->print("\t\t");
            print += "\t}\n";
        }
    }

    // Geoset properties
    if (materialID >= 0) print += "\tMaterialID " + QByteArray::number(materialID) + ",\n";
    if (selectionGroup >= 0) print += "\tSelectionGroup " + QByteArray::number(selectionGroup) + ",\n";
    if (unselectable) print += "\tUnselectable,\n";

    print += "}\n";

    return print;
}
