__Obsolete. See [Model Optimizer and Converter](https://gitlab.com/EzraZebra/model-optimizer-and-converter).__

Optimizer for WC3 .mdl files. More info and download [here](https://www.hiveworkshop.com/threads/mdl-optimizer-v0-1.323265/).
