#ifndef MODEL_H
#define MODEL_H

#include "geoset.h"
#include "sequence.h"

#include <QThread>

class Model : public QObject
{
    Q_OBJECT

    // Model data
    Common *c;
    std::vector<Geoset*> geosets;
    std::vector<Sequence*> sequences;
    std::vector<QByteArrayList> ignoredLines;

    // Parsing data
    QString chunk, object;
    int geosetPos = -1, seqPos = -1;
    size_t chunkTypeCount = 0;
    QString error;

    // Options
    const bool recursive, fakeTris, freeVtcs, idVtcs, unusedMtcs, excSeqExts;

    // Process
    bool line(QByteArray line);
    bool optimize();
    const QByteArrayList print() const;

public:
    Model(bool recursive, int maxPrec,
          bool fakeTris, bool freeVtcs, bool idVtcs, bool unusedMtcs, bool excSeqExts)
        : c(new Common(maxPrec)), recursive(recursive),
          fakeTris(fakeTris), freeVtcs(freeVtcs), idVtcs(idVtcs), unusedMtcs(unusedMtcs), excSeqExts(excSeqExts) {}

public slots:
    void process(const QString &path);

signals:
    void status(const QString &msg, bool success=false, bool finished=false, bool noOpt=false);
};

#endif // MODEL_H
