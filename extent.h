#ifndef EXTENT_H
#define EXTENT_H

#include "common.h"

#include <array>

class Extent
{
    Common *c;
    std::array<QByteArray, 3> minimumExtent, maximumExtent;
    QByteArray boundsRadius;

public:
    Extent(Common *c) : c(c) {}

    bool line(const QByteArray &prop, const QByteArrayList &list) {
        if (prop == "MinimumExtent")
            minimumExtent = { c->round(list.first()), c->round(list.at(1)), c->round(list.last()) };
        else if (prop == "MaximumExtent")
            maximumExtent = { c->round(list.first()), c->round(list.at(1)), c->round(list.last()) };
        else if (prop == "BoundsRadius")
            boundsRadius = c->round(list.first());
        else return false;

        return true;
    }

    const QByteArrayList print(const QByteArray &tabs) const
    {
        QByteArrayList print;
        if (!minimumExtent.empty())
            print += tabs + "MinimumExtent { " + minimumExtent.at(0) + ", " + minimumExtent.at(1) + ", " + minimumExtent.at(2) + " },\n";
        if (!maximumExtent.empty())
            print += tabs + "MaximumExtent { " + maximumExtent.at(0) + ", " + maximumExtent.at(1) + ", " + maximumExtent.at(2) + " },\n";
        if (!boundsRadius.isEmpty())
            print += tabs + "BoundsRadius " + boundsRadius + ",\n";

        return print;
    }
};

#endif // EXTENT_H
