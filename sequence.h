#ifndef SEQUENCE_H
#define SEQUENCE_H

#include "extent.h"

class Sequence
{
    // Sequence data
    Common *c;
    QByteArray name;
    QByteArray start, end, moveSpeed, rarity;
    Extent *extent;
    bool nonLooping=false;

    // Parsing data
    QString error_;

public:
    Sequence(Common *c) : c(c), extent(new Extent(c)) {}

    // Results getters
    inline const QString &error() const { return error_; }

    // Process
    bool line(QByteArray &line);
    const QByteArrayList print() const;
};

#endif // SEQUENCE_H
