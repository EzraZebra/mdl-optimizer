#include "mainwindow.h"
#include "model.h"

#include <QSettings>
#include <QVBoxLayout>
#include <QPushButton>
#include <QFileDialog>
#include <QGroupBox>
#include <QLabel>
#include <QLineEdit>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    QSettings settings("MDL Optimizer", "MDL Optimizer");

    setWindowTitle("MDL Optimizer");
    if (!settings.value("geometry").toByteArray().isEmpty())
        restoreGeometry(settings.value("geometry").toByteArray());

    setCentralWidget(new QWidget());
    auto *centralLyt = new QVBoxLayout(centralWidget());

    // Path input/browse
        auto *browseLyt = new QHBoxLayout();
        centralLyt->addLayout(browseLyt);
        mdlPathEdit = new QLineEdit();
        disableWidgets += mdlPathEdit;
        browseLyt->addWidget(mdlPathEdit);
        mdlPathEdit->setText(settings.value("Path").toString());

        auto *mdlPathBtn = new QPushButton("Browse");
        disableWidgets += mdlPathBtn;
        browseLyt->addWidget(mdlPathBtn);
        connect(mdlPathBtn, &QPushButton::clicked, this, &MainWindow::browseMdl);

    // Options checkboxes
        auto *checkboxLyt = new QGridLayout();
        centralLyt->addLayout(checkboxLyt);

        // General
            auto *generalGrp = new QGroupBox();
            disableWidgets += generalGrp;
            auto *generalGrpLyt = new QGridLayout();
            checkboxLyt->addWidget(generalGrp, 0, 0);
            generalGrp->setLayout(generalGrpLyt);
            generalGrp->setTitle("General");

            recursive = new QCheckBox("Recursive");
            generalGrpLyt->addWidget(recursive, 0, 0);
            recursive->setToolTip("Repeat optimization until no more changes occur.");
            recursive->setChecked(settings.value("Recursive", true).toBool());

            auto *roundNumbersLyt = new QHBoxLayout();
            generalGrpLyt->addLayout(roundNumbersLyt, 1, 0);
            roundNumbers = new QCheckBox("Round numbers to");
            roundNumbersLyt->addWidget(roundNumbers);
            roundNumbers->setToolTip("Round numbers to a maximum of x decimals.\n"
                                     "Note: only applies to Geosets at the moment.");
            roundNumbers->setChecked(settings.value("RoundNumbers", false).toBool());
            roundNumbersSelect = new QComboBox();
            roundNumbersLyt->addWidget(roundNumbersSelect);
            for (int i = 3; i < 10; i++) roundNumbersSelect->addItem(QString::number(i), i);
            roundNumbersSelect->setCurrentIndex(settings.value("RoundNumbersSelect", 3).toInt());
            QLabel *roundNumbersLbl = new QLabel();
            roundNumbersLyt->addWidget(roundNumbersLbl);
            roundNumbersLbl->setText("decimals");

        // Geosets
            auto *geosetGrp = new QGroupBox();
            disableWidgets += geosetGrp;
            auto *geosetGrpLyt = new QGridLayout();
            checkboxLyt->addWidget(geosetGrp, 0, 1);
            geosetGrp->setLayout(geosetGrpLyt);
            geosetGrp->setTitle("Geosets");

            deleteFakeTriangles = new QCheckBox("Delete Fake Triangles");
            geosetGrpLyt->addWidget(deleteFakeTriangles, 0, 0);
            deleteFakeTriangles->setToolTip("Deletes triangles that don't have three unique vertices.\n"
                                            "Note: at least one triangle will be left to avoid deleting geosets for now.");
            deleteFakeTriangles->setChecked(settings.value("DeleteFakeTriangles", false).toBool());

            deleteFreeVertices = new QCheckBox("Delete Free Vertices");
            geosetGrpLyt->addWidget(deleteFreeVertices, 0, 1);
            deleteFreeVertices->setToolTip("Deletes vertices that aren't part of any triangles.");
            deleteFreeVertices->setChecked(settings.value("DeleteFreeVertices", true).toBool());

            mergeIdenticalVertices = new QCheckBox("Merge Identical Vertices");
            geosetGrpLyt->addWidget(mergeIdenticalVertices, 1, 0);
            mergeIdenticalVertices->setToolTip("Merges vertices with the same vertex, normal and texture vertex coordinates.");
            mergeIdenticalVertices->setChecked(settings.value("MergeIdenticalVertices", true).toBool());

            deleteUnusedMatrices = new QCheckBox("Delete Unused Matrices");
            geosetGrpLyt->addWidget(deleteUnusedMatrices, 1, 1);
            deleteUnusedMatrices->setToolTip("Deletes matrices (groups of bone IDs) that aren't connected to any vertices.");
            deleteUnusedMatrices->setChecked(settings.value("DeleteUnusedMatrices", true).toBool());

            deleteExcessSeqExtents = new QCheckBox("Delete Excess Sequence Extents");
            geosetGrpLyt->addWidget(deleteExcessSeqExtents, 2, 0);
            deleteExcessSeqExtents->setToolTip("Deletes sequence extents if they exceed the amount of sequences.");
            deleteExcessSeqExtents->setChecked(settings.value("DeleteExcessSeqExtents", true).toBool());

    // Optimize button
        QPushButton *optimizeBtn = new QPushButton("Optimize");
        disableWidgets += optimizeBtn;
        centralLyt->addWidget(optimizeBtn);
        connect(optimizeBtn, &QPushButton::clicked, this, &MainWindow::optimize);

    // Status text
        statusText = new QTextEdit();
        centralLyt->addWidget(statusText);
        statusText->setReadOnly(true);
}

MainWindow::~MainWindow()
{
    QSettings settings("MDL Optimizer", "MDL Optimizer");

    settings.setValue("Path", mdlPathEdit->text());

    settings.setValue("Recursive", recursive->isChecked());
    settings.setValue("RoundNumbers", roundNumbers->isChecked());
    settings.setValue("RoundNumbersSelect", roundNumbersSelect->currentIndex());

    settings.setValue("DeleteFakeTriangles", deleteFakeTriangles->isChecked());
    settings.setValue("DeleteFreeVertices", deleteFreeVertices->isChecked());
    settings.setValue("MergeIdenticalVertices", mergeIdenticalVertices->isChecked());
    settings.setValue("DeleteUnusedMatrices", deleteUnusedMatrices->isChecked());
    settings.setValue("DeleteExcessSeqExtents", deleteExcessSeqExtents->isChecked());

    settings.setValue("geometry", saveGeometry());

    if (modelThread.isRunning()) modelThread.quit();
}

void MainWindow::statusError(const QString &msg) const
{
    statusText->append("<span style='color:red; font-weight:bold;'>" + msg + "</span>");
}

void MainWindow::statusNotice(const QString &msg) const
{
    statusText->append("<span style='color:blue; font-weight:bold;'>" + msg + "</span>");
}

void MainWindow::statusSuccess(const QString &msg) const
{
    statusText->append("<span style='color:green; font-weight:bold;'>" + msg + "</span>");
}

void MainWindow::statusSeparator() const
{
    statusText->append("<br />----------------------<br />");
}

void MainWindow::modelStatus(const QString &msg, bool success, bool finished, bool noOpt)
{
    if (success) statusSuccess(msg);
    else if (!finished) statusNotice(msg);
    else statusError(msg);

    if (finished || noOpt) {
        statusSeparator();
        modelThread.quit();
        modelThread.wait();
        for (auto &w : disableWidgets) w->setDisabled(false);
    }
}

void MainWindow::browseMdl()
{
    QString path = QFileDialog::getOpenFileName(this, tr("Browse Model"), mdlPathEdit->text(), tr("Models (*.mdl)"));
    if (!path.isEmpty()) mdlPathEdit->setText(QDir::toNativeSeparators(path));
}

void MainWindow::optimize()
{
    for (auto &w : disableWidgets) w->setDisabled(true);

    bool roundNumbersCh = roundNumbers->isChecked();
    bool deleteFakeTrianglesCh = deleteFakeTriangles->isChecked();
    bool deleteFreeVerticesCh = deleteFreeVertices->isChecked();
    bool mergeIdenticalVerticesCh = mergeIdenticalVertices->isChecked();
    bool deleteUnusedMatricesCh = deleteUnusedMatrices->isChecked();
    bool deleteExcessSeqExtentsCh = deleteExcessSeqExtents->isChecked();

    // Nothing checked
    if (!(roundNumbersCh
          || deleteFakeTrianglesCh || deleteFreeVerticesCh || mergeIdenticalVerticesCh || deleteUnusedMatricesCh || deleteExcessSeqExtentsCh)) {
        statusNotice("Please select at least one option.");
        return;
    }

    QString result;
    QFileInfo mdlFO(mdlPathEdit->text());

    // Check file
    if (mdlPathEdit->text().isEmpty()) result = "Path is empty.";
    else if (!mdlFO.exists()) result = "File does not exist.";
    else if (mdlFO.suffix() != "mdl" && mdlFO.suffix() != "mdx") result = "Please choose a .mdx or .mdl file.";
    else if (!mdlFO.isReadable()) result = "File is not readable.";

    if (!result.isEmpty()) {
        statusNotice(result);
        for (auto &w : disableWidgets) w->setDisabled(false);
    }
    else {
        statusSeparator();

        auto *model = new Model(recursive->isChecked(), roundNumbersCh ? roundNumbersSelect->currentData().toInt() : 0,
                                deleteFakeTrianglesCh, deleteFreeVerticesCh, mergeIdenticalVerticesCh,
                                deleteUnusedMatricesCh, deleteExcessSeqExtentsCh);
        model->moveToThread(&modelThread);

        connect(&modelThread, &QThread::finished, model, &QObject::deleteLater);
        connect(this, &MainWindow::loadModel, model, &Model::process);
        connect(model, &Model::status, this, &MainWindow::modelStatus);

        modelThread.start();
        emit loadModel(mdlFO.absoluteFilePath());
    }
}
