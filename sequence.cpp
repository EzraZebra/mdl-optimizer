#include "sequence.h"

/** Convert line to Sequence data. Expects a trimmed line.
 * Return true on error or when last line is reached. */
bool Sequence::line(QByteArray &line)
{
    // Close the current scope
    if (line == "}") return true;

    int spacePos = line.indexOf(" ");
    QByteArray prop = line.right(line.size()-spacePos-1);
    QByteArrayList list = Common::getList(prop);
    prop = line.left(spacePos);

    if (prop == "Anim") name = list.first().trimmed();
    else if (prop == "Interval") {
        start = c->round(list.first());
        end = c->round(list.last());
    }
    else if (line.startsWith("NonLooping")) nonLooping = true;
    else if (prop == "MoveSpeed") moveSpeed = c->round(list.first());
    else if (prop == "Rarity") rarity = c->round(list.first());
    else if (!extent->line(prop, list)) {
        error_ = "Unknown Sequence property: " + prop;
        return true;
    }

    return false;
}

/** Return a QByteArrayList with the sequence for outputting to a .mdl file */
const QByteArrayList Sequence::print() const
{
    QByteArrayList print;

    print += "\tAnim \"" + name + "\" {\n";
    print += "\t\tInterval { " + start + ", " + end + " },\n";
    if (nonLooping) print += "\t\tNonLooping,\n";
    if (!rarity.isEmpty()) print += "\t\tRarity " + rarity + ",\n";
    if (!moveSpeed.isEmpty()) print += "\t\tMoveSpeed " + moveSpeed + ",\n";

    QByteArrayList ext = extent->print("\t\t");
    if (!ext.isEmpty()) print += ext;

    return print;
}
