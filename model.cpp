#include "model.h"

#include <QDir>
#include <QTextStream>
#include <QProcess>
#include <QCoreApplication>
#include <QDataStream>
#include <fstream>
#include <ios>

bool Model::line(QByteArray line)
{
    bool success = true;
    QByteArray lineTrimmed = line.trimmed();

    // Start new chunk or write to ignored lines
    if (chunk.isEmpty()) {
        if (lineTrimmed.startsWith("Geoset ")) {
            chunk = "Geoset";
            geosets.push_back(new Geoset(c));
            if (geosetPos == -1) {
                geosetPos = ignoredLines.size();
                chunkTypeCount++;
            }
        }
        else if (lineTrimmed.startsWith("Sequences")) {
            chunk = "Sequences";
            if (seqPos == -1) {
                seqPos = ignoredLines.size();
                chunkTypeCount++;
            }
        }
        else if (ignoredLines.empty()) ignoredLines.push_back(QByteArrayList()<<(line));
        else ignoredLines.back() += line;
    }
    // Feed Geoset
    else {
        if (chunk == "Geoset") {
            if (geosets.back()->line(lineTrimmed)) {
                if (geosets.back()->error().isEmpty())  chunk = "";
                else {
                    error = geosets.back()->error();
                    success = false;
                }
            }
        }
        // Feed Sequences
        else if (chunk == "Sequences") {
            if (lineTrimmed.startsWith("Anim")) sequences.push_back(new Sequence(c));

            if (sequences.back()->line(lineTrimmed)) {
                if (sequences.back()->error().isEmpty()) chunk = "";
                else {
                    error = sequences.back()->error();
                    success = false;
                }
            }
        }
        else {
            error = "Unknown chunk: " + chunk + ".";
            success = false;
        }

        if (chunk == "") ignoredLines.push_back(QByteArrayList());
    }

    return success;
}

void Model::process(const QString &path)
{
    QFile mdlFile(path);

    // Can't read file
    if (!mdlFile.open(QIODevice::ReadOnly | QIODevice::Text)) {
        emit status("Failed to read file.", false, true);
        return;
    }

    QFileInfo mdlFO(path);

    emit status("Loading " + mdlFO.fileName() + " ...");

    // Start reading file
    bool success = true;
    int lineNr = 0;
    for (; !mdlFile.atEnd() && success; lineNr++) {
        success = line(mdlFile.readLine());
    }

    mdlFile.close();

    QString errMsg;
    if (success) errMsg = "Model successfully loaded.";
    else errMsg = "<br />Line " + QString::number(lineNr) + ": " + error;

    emit status(errMsg, success, !success);

    // Check for error & optimize
    if (success && optimize()) {
        QString basePath(mdlFO.canonicalPath() + "/" + mdlFO.completeBaseName() + "_opt");
        mdlFO.setFile(basePath+".mdl");

        // Find unused filename
        for (int i = 2; mdlFO.exists(); i++)
            mdlFO.setFile(basePath+QString::number(i)+".mdl");

        basePath = mdlFO.absoluteFilePath();
        mdlFile.setFileName(basePath);

        // Write new file
        if(!mdlFile.open(QIODevice::WriteOnly)) {
            errMsg = "Failed to create a new file.";
            success = false;
        }
        else {
            status("Writing new file...");

            QTextStream mdlStream(&mdlFile);

            int n = 1;
            for(auto const& i : print()) {
                mdlStream << i;
                n++;

                if (mdlStream.status() != QTextStream::Ok) {
                    errMsg = "Line " + QString::number(n) + ": Error while writing to " + mdlFO.fileName();
                    success = false;
                    break;
                }
            }
        }

        if (success) errMsg = "Optimized model saved as:<br />" + QDir::toNativeSeparators(basePath);
        emit status(errMsg, success, true);
    }
}

bool Model::optimize()
{
    emit status("<br />Optimizing model ...");

    int diffTris, diffVtcs, diffMtcs, diffSeqExts;
    bool changesMade, anyChanges = false;

    int i = 1;
    do {
        if (i > 1) emit status("Pass " + QString::number(i) + "...");
        changesMade = false;

        // Optimize geosets if selected
        if (fakeTris || freeVtcs || idVtcs) {
            diffTris = 0, diffVtcs = 0, diffMtcs = 0, diffSeqExts = 0;

            for (auto geo : geosets) {
                changesMade = geo->optimize(fakeTris, freeVtcs, idVtcs, unusedMtcs, excSeqExts ? sequences.size() : -1)
                                || changesMade;
                diffTris += geo->diffTris();
                diffVtcs += geo->diffVtcs();
                diffMtcs += geo->diffMtcs();
                diffSeqExts += geo->diffSeqExts();
            }
        }

        i++;
        anyChanges = anyChanges || changesMade;
    } while (recursive && changesMade);

    // Emit result
    QString result;
    if (anyChanges) {
        if (c->numsRounded() != 0) result += QString::number(c->numsRounded()) + " numbers rounded<br />";
        if (diffTris != 0) result += "Triangles reduced by " + QString::number(diffTris) + "<br />";
        if (diffVtcs != 0) result += "Vertices reduced by " + QString::number(diffVtcs) + "<br />";
        if (diffMtcs != 0) result += "Matrices reduced by " + QString::number(diffMtcs) + "<br />";
        if (diffSeqExts != 0) result += "Sequence Extents reduced by " + QString::number(diffSeqExts)
                                + " (" + QString::number(diffSeqExts/geosets.size()) + " per geoset)<br />";
    }
    else result = "<br />Nothing to optimize.";

    emit status(result, anyChanges, false, !anyChanges);
    return anyChanges;
}

/**
  Return a QByteArrayList with the model for outputting to a .mdl file
 * @brief Model::print
 * @return
 */
const QByteArrayList Model::print() const
{
    QByteArrayList print;
    size_t chunkTypesLeft = chunkTypeCount;

    for (size_t i = 0; i < ignoredLines.size()+chunkTypesLeft; i++) {
        if ((int)i == geosetPos) {
            for (auto geo : geosets) print += geo->print();
            if (chunkTypesLeft > 0) chunkTypesLeft--;
        }
        if ((int)i == seqPos) {
            print += "Sequences " + QByteArray::number(sequences.size()) + " {\n";
            for (auto seq : sequences) print += seq->print();
            print += "}\n";

            if (chunkTypesLeft > 0) chunkTypesLeft--;
        }

        if (i < ignoredLines.size()) print += ignoredLines.at(i);
    }

    return print;
}
