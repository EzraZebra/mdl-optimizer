#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QCheckBox>
#include <QTextEdit>
#include <QComboBox>
#include <QThread>

class MainWindow : public QMainWindow
{
    Q_OBJECT

    QLineEdit   *mdlPathEdit;
    QCheckBox   *recursive, *roundNumbers,
                *deleteFakeTriangles, *deleteFreeVertices, *mergeIdenticalVertices, *deleteUnusedMatrices, *deleteExcessSeqExtents;
    QTextEdit   *statusText;
    QComboBox   *roundNumbersSelect;

    QList<QWidget*> disableWidgets;

    QThread modelThread;

    inline void statusError(const QString &msg) const;
    inline void statusNotice(const QString &msg) const;
    inline void statusSuccess(const QString &msg) const;
    inline void statusSeparator() const;

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void browseMdl();
    void optimize();
    void modelStatus(const QString &msg, bool success, bool finished, bool noOpt);

signals:
    void loadModel(const QString &path);
};

#endif // MAINWINDOW_H
