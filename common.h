#ifndef COMMON_H
#define COMMON_H

#include <QByteArrayList>

class Common
{
    const int maxPrec;
    int numsRounded_ = 0;

public:
    Common(int maxPrec) : maxPrec(maxPrec) {};
    int numsRounded() const { return numsRounded_; }

    /** Convert line to QByteArrayList of values */
    inline static const QByteArrayList getList(QByteArray &line)
    {
        return line.replace(" ", "").replace("{", "").replace("},", "").replace('"', "").split(',');
    }

    /** Convert QByteArray to a pair of a double and its precision */
    const QByteArray round(QByteArray bytearray)
    {
        if (bytearray.isEmpty()) return "0";
        else if (!bytearray.contains(".")) return bytearray;
        else {
            int prec = maxPrec;

            if (prec < 3) {
                bool e = bytearray.contains("e");
                if (!e && !bytearray.contains("E")) prec = bytearray.size()-bytearray.indexOf(".")-1;
                else {
                    QByteArrayList list = e ? bytearray.split('e') : bytearray.split('E');
                    prec = list.first().size()-list.first().indexOf(".")-1 - list.last().toInt();
                }
            }
            else numsRounded_++;

            bytearray = QByteArray::number(bytearray.toDouble(), 'f', prec);

            while (bytearray.endsWith("0")) bytearray.chop(1);
            if (bytearray.endsWith(".")) bytearray.chop(1);

            return bytearray;
        }
    }
};

#endif // COMMON_H
